-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Sep 02, 2016 at 04:37 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `longevo`
--

-- --------------------------------------------------------

--
-- Table structure for table `chamados`
--

CREATE TABLE `chamados` (
  `id` int(12) NOT NULL,
  `cliente` int(11) DEFAULT NULL,
  `pedido` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `observation` longtext NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chamados`
--

INSERT INTO `chamados` (`id`, `cliente`, `pedido`, `title`, `observation`, `date`) VALUES
(1, 1, 1, 'Pedido cancelado', 'Produto veio errado.', '2016-09-02 03:42:41'),
(2, 2, 2, 'Produto errado', 'Compra do produto 1 e envio do produto 2.', '2016-09-02 03:45:38'),
(3, 1, 2, 'Produto ainda com defeito', 'Recebi o produto e pela segunda vez veio quebrado.', '2016-09-02 03:46:35'),
(4, 3, 2, 'ok', 'ok', '2016-09-02 05:39:50'),
(5, 4, 1, '5 Teste de envio', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2016-09-02 06:09:39'),
(13, 5, 1, 'E-Corp', 'fail.', '2016-09-02 06:51:07'),
(14, 6, 2, 'Testando sistema', 'Teste do sistema de atendimento.', '2016-09-02 14:44:54'),
(15, 6, 2, 'Testando sistema', 'Teste do sistema de atendimento.', '2016-09-02 14:46:59'),
(16, 7, 2, 'Testando sistema', 'Teste do sistema de atendimento.', '2016-09-02 14:47:33'),
(17, 8, 1, '10- Teste', 'Como esta funcionando o sistema?', '2016-09-02 14:54:09'),
(18, 9, 1, 'Maria teste', 'Testando aqui o sistema.', '2016-09-02 15:12:58'),
(19, 10, 2, 'Ok', 'Sistema ok.', '2016-09-02 15:13:48'),
(20, 1, 1, 'seis registros', 'teste', '2016-09-02 16:18:58');

-- --------------------------------------------------------

--
-- Table structure for table `clientes`
--

CREATE TABLE `clientes` (
  `id` int(12) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clientes`
--

INSERT INTO `clientes` (`id`, `name`, `email`) VALUES
(1, 'Everlon Passos', 'everlon@worium.com.br'),
(2, 'Thiago', 'thiago@gmail.com'),
(3, 'Elen', 'elen@yahoo.com'),
(4, 'Augusto Lima', 'a.lima@gmail.com'),
(5, 'Everlon Passos', 'dev@everlon.com.br'),
(6, 'Luiza', 'luiza@outlook.com'),
(7, 'Teresinha', 'tereza@outlook.com'),
(8, 'Inácio', 'inacio@hotmail.com'),
(9, 'Maria', 'maria@gmail.com'),
(10, 'João Pereira', 'jpereira@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `pedidos`
--

CREATE TABLE `pedidos` (
  `id` int(12) NOT NULL,
  `number` int(11) DEFAULT NULL,
  `client` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pedidos`
--

INSERT INTO `pedidos` (`id`, `number`, `client`, `date`) VALUES
(1, 90099, 1, '2016-09-01 09:16:31'),
(2, 800, NULL, '2016-08-03 15:38:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chamados`
--
ALTER TABLE `chamados`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chamados`
--
ALTER TABLE `chamados`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;