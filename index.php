<?php
/**
 * Author: Everlon Passos (dev@everlon.com.br)
 * Date: 01/09/2016 20:04:03
 */

require_once __DIR__.'/config/config.php';
require_once __DIR__.'/config/routes.php';


$app->run();