<?php
/**
 * Author: Everlon Passos (dev@everlon.com.br)
 * Date: 01/09/2016 20:06:28
 */
    $atendimento = $app['controllers_factory'];



    $atendimento->get('/novo', function() use ($app) {
        return $app['twig']->render('sac-form.twig' );
    })
        ->bind('new');



    $atendimento->post('/save', function ($qnt,$pag) use ($app) {
        $qnt = (!$app['request']->query->get('qnt')) ? 5 : (int)$app['request']->query->get('qnt') ;
        $pag = (!$app['request']->query->get('pag')) ? 0 : (int)$app['request']->query->get('pag') ;

        # Verifica se pedido existe
        $number  = $app->escape($app['request']->get('number'));
        $pedidos = $app['db']::findOne( 'pedidos', ' number = ? ', [ $number ] );
        if (!is_object($pedidos)) {
            return $app['twig']->render('sac.twig', ['msg'=>'Pedido não encontrado.'] );
        }

        # Verifica se cliente já tem cadastro
        $email   = $app->escape($app['request']->get('email'));
        $cliente = $app['db']::findOne( 'clientes', ' email = ? ', [ $email ] );

        if (!is_object($cliente)) {
            # Se não existir o email, é cadastrado / O pedido deveria ser relacionado com o pedido
            $post        = $app['db']::dispense('clientes');
            $post->name  = $app->escape($app['request']->get('name'));
            $post->email = $app->escape($app['request']->get('email'));
            $client_id   = $app['db']::store($post);
        } else {
            $client_id   = $cliente->id;
        }

        $post = $app['db']::dispense('chamados');
        $post->cliente     = $client_id;
        $post->pedido      = $pedidos->id;
        $post->title       = $app->escape($app['request']->get('title'));
        $post->observation = $app->escape($app['request']->get('observation'));
        $post->date        = date("Y-m-d H:i:s");
        $return_id         = $app['db']::store($post);

        return $app->redirect( $app["url_generator"]->generate("home") );
    })
        ->value('qnt', 5)
        ->value('pag', 0)
        ->bind('sac_submit');


    # Json para Ajax
    $atendimento->post('/get_number', function() use ($app) {
        $number = $app->escape($app['request']->get('number'));
        $data   = $app['db']::findOne( 'pedidos', ' number = ? ', [ $number ] );
        if (is_object($data)) {
            return $app->json(['error'=>false, 'data'=>$data], 200, ['Content-Type'=>'text/json']);
        } else {
            return $app->json(['error'=>true], 200, ['Content-Type'=>'text/json']);
        }
    });


    $atendimento->match('/', function($qnt,$pag) use ($app) {
        $qnt = (!$app['request']->query->get('qnt')) ? 5 : (int)$app['request']->query->get('qnt') ;
        $pag = (!$app['request']->query->get('pag')) ? 0 : (int)$app['request']->query->get('pag')*$qnt ;

        # Busca
        $busca = $app['request']->get('busca');
        if (!empty($busca)) {
            $sql  = "SELECT clientes.*, pedidos.*, chamados.* FROM chamados JOIN clientes, pedidos WHERE (clientes.email LIKE '%$busca%' OR pedidos.number LIKE '%$busca%') AND (clientes.id = chamados.cliente AND pedidos.id = chamados.pedido) ";
            $data = $app['db']::getAll($sql);
            $msg  = "Resultado da busca: ".count($data);
        } else {
            $data = $app['db']::findAll( 'chamados', ' LIMIT :pag, :qnt ', [ ':pag'=>$pag, ':qnt'=>$qnt ] );
            $data = $app['db']::exportAll( $data, TRUE ); /* Padronizando: Converter para array */
        }

        # Agrupar com os clientes
        foreach ($data as $key => $chamado) {
            $cliente = $app['db']::findOne( 'clientes', ' id = ? ', [ $chamado['cliente'] ] );
            if ($chamado['cliente'] == $cliente->id) {
                $data[$key]['cliente']  = $cliente->name;
                $data[$key]['email'] = $cliente->email;
            }
            $pedido  = $app['db']::findOne( 'pedidos', ' id = ? ', [ $chamado['pedido'] ] );
            if ($chamado['pedido'] == $pedido->id) {
                $data[$key]['pedido']  = $pedido->number;
            }
            $data[$key]['pedido'] = str_pad($data[$key]['pedido'], 5, "0", STR_PAD_LEFT);
        }
        $pagination = [ 'count' => count($app['db']::findAll('chamados')), 'pag'=>(int)$app['request']->query->get('pag')+1, 'qnt'=>$qnt ];

        return $app['twig']->render('sac.twig', ['data'=>$data, 'msg'=>$msg, 'pagination'=>$pagination] );
    }, 'GET|POST')
        ->value('qnt', 5)
        ->value('pag', 0)
        ->bind('home');



return $atendimento;