<?php
/**
 * Author: Everlon Passos (dev@everlon.com.br)
 * Date: 01/09/2016 20:04:03
 */

    require_once __DIR__.'/../src/vendor/autoload.php';

    # Iniciando SILEX
    $app = new Silex\Application();
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Response;

    $app->register( new Silex\Provider\UrlGeneratorServiceProvider() );


    # Configurações TWIG
    $app->register(new Silex\Provider\TwigServiceProvider(),
                array(
                    'debug'     => true,
                    // 'cache' => 'cache',
                    'twig.path' => array( __DIR__.'/../src/views' )
            ));


    define('ASSETS', 'http://localhost/silex-longevo/assets');
    $app['assets'] = array(
        'js'  => ASSETS.'/js',
        'css' => ASSETS.'/css',
        'img' => ASSETS.'/imgs',
    );



    # Configuração RedBeanPHP
    $app['db'] = $app->share(function() use ($app) {
            $mysqlHost     = "localhost";
            $mysqlUser     = "root";
            $mysqlPassword = "root";

        $R = new \RedBeanPHP\Facade;
        //$R::setup('pgsql:host='.$mysqlHost.'; dbname=longevo', $mysqlUser, $mysqlPassword);
        $R::setup('mysql:host='.$mysqlHost.'; dbname=longevo', $mysqlUser, $mysqlPassword);
        $R::setAutoResolve( true );
        return $R;
    });


    $app['debug'] = true;